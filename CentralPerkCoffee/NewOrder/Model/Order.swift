//
//  Order.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 19/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

enum CoffeeType: String, Codable, CaseIterable {
    case capuccino
    case latte
    case mocaccino
    case espresso
    case cortado
    case mitad
    case sombra
    case nube
    case frappe
    case manchado
    case largo
    case doppio
}

enum CoffeeSize: String, Codable, CaseIterable {
    case small
    case medium
    case large
}

struct NewOrder: Codable {
    let name: String
    let email: String
    let type: CoffeeType
    let size: CoffeeSize
}

// In case that the keys does not matches with the params from API use CodingKeys

//enum CodingKeys: String, CodingKey {
//    case name
//    case email
//    case type = ""
//    case size = ""
//}
