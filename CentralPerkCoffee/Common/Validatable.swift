//
//  Validatable.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 19/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

protocol Validatable {
    func validate() -> Bool
}
