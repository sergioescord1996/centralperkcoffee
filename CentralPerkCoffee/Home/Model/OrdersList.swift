//
//  OrdersList.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 20/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

struct OrdersList: Codable {
    private var ordersList: [Order]
    private var filteredOrdersList: [Order]

    init() {
        ordersList = []
        filteredOrdersList = []
    }
    
    init(_ orders: [Order]) {
        self.ordersList = orders
        self.filteredOrdersList = orders
    }

    mutating func filterOrders(by name: String) {
        if name.isEmpty {
            filteredOrdersList = ordersList
        } else {
            filteredOrdersList = ordersList.filter({ (order) -> Bool in
                return order.name.contains(name)
            })
        }
    }

    func getOrders() -> [Order] {
        return filteredOrdersList
    }
    
    func getOrder(at position: Int) -> Order {
        return filteredOrdersList[position]
    }
}

struct Order: Codable {
    var name: String
    var date: String
}
