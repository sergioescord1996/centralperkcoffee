//
//  HomeRouter.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 20/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

class HomeRouter {
    
    var viewController: UIViewController {
        return getViewController()
    }
    
    private var sourceView: UIViewController?
    
    private func getViewController() -> UIViewController {
        let viewController = HomeViewController(nibName: "HomeViewController", bundle: Bundle.main)
        return viewController
    }
    
    func setSourceView(_ sourceView: UIViewController?) {
        guard let sourceView = sourceView else { fatalError("Error setting source view in \(String(describing: self))") }
        self.sourceView = sourceView
    }
    
    //TODO: Navigate to AddOrder
}
