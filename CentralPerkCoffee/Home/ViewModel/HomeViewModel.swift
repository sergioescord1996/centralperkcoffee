//
//  HomeViewModel.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 19/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

public class HomeViewModel {
    
    private var ordersList: OrdersList = OrdersList()
    private let localDataManager = LocalDataManager()
    
    func getOrders() -> Observable<[Order]> {
        return localDataManager.getOrders()
    }
    
    func setData(_ orders: [Order]) {
        ordersList = OrdersList(orders)
    }
    
    func getFilteredOrders() -> [Order] {
        return ordersList.getOrders()
    }
    
    func filterOrders(by name: String) {
        ordersList.filterOrders(by: name)
    }
    
    func getOrder(at position: Int) -> Order {
        return ordersList.getOrder(at: position)
    }
}
