//
//  HomeViewController.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 20/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    
    private let viewModel = HomeViewModel()
    private let disposeBag = DisposeBag()
    
    lazy var searchController: UISearchController = createSearchController()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUIElements()
        
        loadData()
    }
    
    private func configureUIElements() {
        
        navigationItem.title = "List of orders"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        configureSearchBarController()
        
        activity.startAnimating()
        activity.hidesWhenStopped = true
    }
    
    private func reloadTableView() {
        DispatchQueue.main.async { [weak self] in
            self?.activity.stopAnimating()
            self?.tableView.reloadData()
        }
    }
    
    private func createSearchController() -> UISearchController {
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .default
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "Search order"
        
        return controller
    }
    
    private func configureSearchBarController() {
        let searchBar = searchController.searchBar
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
        
        searchBar.rx.text
            .orEmpty
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { result in
                self.viewModel.filterOrders(by: result)
                self.reloadTableView()
            }, onError: { (error) in
                
            }, onCompleted: {}).disposed(by: disposeBag)
    }
    
    private func loadData() {
        viewModel.getOrders()
            .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .default))
            .observe(on: ConcurrentDispatchQueueScheduler.init(qos: .default))
            .subscribe(onNext: { orders in
                self.viewModel.setData(orders)
                self.reloadTableView()
            }, onError: { error in
                print("Error fetching data of orders")
            }, onCompleted: {}).disposed(by: disposeBag)
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getFilteredOrders().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let order = viewModel.getOrder(at: indexPath.row)
        
        cell.textLabel?.text = "\(order.name) - \(order.date)"
        cell.accessoryType = .detailButton
        
        return cell
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.isActive = false
        reloadTableView()
    }
}
