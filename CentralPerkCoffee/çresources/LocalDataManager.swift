//
//  LocalDataManager.swift
//  CentralPerkCoffee
//
//  Created by Sergio Escalante Ordonez on 20/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation
import RxSwift
import CoreData

class LocalDataManager {
    
    func getOrders() -> Observable<[Order]> {
        return Observable.create { observer in
            let orders = [Order(name: "Primera", date: "Hohy"), Order(name: "Segunda", date: "Mañana"), Order(name: "Tercera", date: "Ayer")]
            
            DispatchQueue.global().asyncAfter(deadline: .now() + 2.0, execute: {
                observer.onNext(orders)
                
                observer.onCompleted()
            })
            
            return Disposables.create()
        }
    }
}
